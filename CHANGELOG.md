# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.4] - 2021-06-27

### Fixed

- Minor fixes to examples and CSS

## [1.0.3] - 2021-06-26

### Changed

- Updated readme to include screenshot and links to the docs/example

## [1.0.2] - 2021-06-26

### Fixed

- Fix Github release action per-version documentation publishing

## [1.0.1] - 2021-06-26

### Fixed

- Fix Github release action

## [1.0.0] - 2021-06-26

### Changed

- Initial Release

[Unreleased]: https://github.com/symbioquine/ol-side-panel/compare/1.0.4...HEAD
[1.0.4]: https://github.com/symbioquine/ol-side-panel/compare/v1.0.3...v1.0.4
[1.0.3]: https://github.com/symbioquine/ol-side-panel/compare/v1.0.2...v1.0.3
[1.0.2]: https://github.com/symbioquine/ol-side-panel/compare/v1.0.1...v1.0.2
[1.0.1]: https://github.com/symbioquine/ol-side-panel/compare/v1.0.0...v1.0.1
[1.0.0]: https://github.com/symbioquine/ol-side-panel/releases/tag/1.0.0
